FROM docker.io/rust:alpine AS build

RUN set -xeuf; \
  apk add --no-cache musl-dev; \
  adduser -D build; \
  :

WORKDIR /build
COPY Cargo.toml Cargo.lock .
COPY src ./src
RUN chown -R build:build /build
USER build
RUN cargo install --path . --root /home/build/install

FROM docker.io/alpine:latest AS run

LABEL "io.containers.capabilities"="CHOWN,DAC_OVERRIDE,FOWNER,FSETID,KILL,NET_BIND_SERVICE,SETFCAP,SETGID,SETPCAP,SETUID,SYS_CHROOT"

RUN set -xeuf; \
  apk add --no-cache buildah; \
  adduser -D build; \
  echo -e "build:1:999\nbuild:1001:64535" > /etc/subuid; \
  echo -e "build:1:999\nbuild:1001:64535" > /etc/subgid; \
  :

COPY --from=build --chmod=755 /home/build/install/bin/wrapper /usr/local/bin/wrapper

COPY storage.conf containers.conf /etc/containers

USER build

ENV BUILDAH_ISOLATION=chroot HOME=/home/build USER=build LOGNAME=build

ENTRYPOINT [ "/usr/local/bin/wrapper" ]
