# woodpecker-buildah

A thin wrapper around buildah to build and run container images inside of a
Woodpecker CI pipeline, and also to make multiarch images.

This should be useable as either a normal woodpecker command image or as a plugin.

## Settings

* `tag`: The only mandatory argument.  This is the tag that is actually built and pushed.
* `commands`: Specifies the commands to run.  Currently, this is `[login, build, push]`.  Options are `build`, `push`, and `manifest`.
* `context`: Specifies an alternate context, which may be relative to the project directory.
* `arguments`: A mapping from arguments (without their leading hyphens) to their values. This is added to all commands.
* `username`: Username for login command.
* `password`: Password for the login command.
* `registry`: Registry to login to.
* `login-arguments`: Arguments to append after `arguments` to `buildah login` commands.
* `build-arguments`: Arguments to append after `arguments` to `buildah build` commands.
* `push-arguments`: Arguments to append after `arguments` to `buildah push` commands.
* `manifest-arguments`: Arguments to append after `arguments` to `buildah manifest` commands.
* `manifest-create-arguments`: Arguments to append after `arguments` and `manifest-arguments` to `buildah manifest create` commands.
* `manifest-add-arguments`: Arguments to append after `arguments` and `manifest-arguments` to `buildah manifest add` commands.
* `manifest-push-arguments`: Arguments to append after `arguments` and `manifest-arguments` to `buildah manifest push` commands.

## Example

This plugin is self-hosting, and builds itself.  [Checking out the woodpecker pipelines is the best way of finding the capabilities of this library](https://codeberg.org/Taywee/woodpecker-buildah/src/branch/master/.woodpecker).

A very simple use might look like this:

```yaml
steps:
  build:
    image: 'docker.io/taywee/woodpecker-buildah:latest'
    pull: true
    secrets: [docker_hub_auth]
    settings:
      tag: 'docker.io/myuser/myimage:latest'
      env-files:
        DOCKER_HUB_AUTH: /home/build/.config/containers/auth.json

```

## Getting this to work in Docker

Running buildah inside a container gets pretty difficult.  We use the
the [buildahimage Containerfile](https://github.com/containers/buildah/blob/04c61a7b7277e44ea69ea93ebbded92fdecac036/contrib/buildahimage/Containerfile)'s changes to get buildah working in a container, and also replace the overlayfs storage driver with the vfs.  vfs is
slower, but doesn't cause errors due to capabilities.

That work is done here, but the running Woodpecker agent also needs some seccomp
changes.  It works if you just set it to the podman ones.  A functioning
`/etc/sysconfig/docker` file might look like this:

```sh
# /etc/sysconfig/docker

# Modify these options if you want to change the way the docker daemon runs
OPTIONS="--selinux-enabled \
  --log-driver=journald \
  --live-restore \
  --default-ulimit nofile=1024:1024 \
  --init-path /usr/libexec/docker/docker-init \
  --userland-proxy-path /usr/libexec/docker/docker-proxy \
  --seccomp-profile /usr/share/containers/seccomp.json \
"
```

## Attribution

The rust code is Copyright 2023 Taylor C. Richberger, released under the Mozilla Public License Version 2.0.

This copies some instructions and files from the [buildahimage Containerfile](https://github.com/containers/buildah/blob/04c61a7b7277e44ea69ea93ebbded92fdecac036/contrib/buildahimage/Containerfile), released under the Apache license.
