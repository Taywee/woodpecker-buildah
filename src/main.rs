use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::env;
use std::env::VarError::NotPresent;
use std::ffi::OsStr;
use std::fmt::Debug;
use std::fs;
use std::io::Write;
use std::path::PathBuf;
use std::process::{Command, Stdio};

#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
enum ArgumentValue {
    Boolean(bool),
    String(String),
    Array(Vec<String>),
}

impl From<String> for ArgumentValue {
    fn from(v: String) -> Self {
        Self::String(v)
    }
}

enum Flag {
    NoValue,
    ValueList(Vec<String>),
}

impl From<ArgumentValue> for Flag {
    fn from(value: ArgumentValue) -> Self {
        match value {
            ArgumentValue::Boolean(b) => {
                if b {
                    Self::NoValue
                } else {
                    Self::ValueList(vec!["false".into()])
                }
            }
            ArgumentValue::String(s) => Self::ValueList(vec![s]),
            ArgumentValue::Array(arr) => Self::ValueList(arr),
        }
    }
}

fn env_var_to_arguments(var_name: &str) -> Result<Vec<String>, Box<dyn std::error::Error>> {
    let arguments: HashMap<String, ArgumentValue> = match env::var(var_name) {
        Err(NotPresent) => return Ok(Default::default()),
        e => serde_json::from_str(&e?)?,
    };

    Ok(arguments
        .into_iter()
        .flat_map(|(k, v)| {
            let flag: Flag = v.into();
            match flag {
                Flag::NoValue => vec![format!("--{k}")],
                Flag::ValueList(list) => list
                    .into_iter()
                    .map(move |value| format!("--{k}={value}"))
                    .collect(),
            }
        })
        .collect())
}

fn login<A, A2>(
    arguments: A,
    username: &str,
    password: &str,
    registry: &str,
) -> Result<(), Box<dyn std::error::Error>>
where
    A: IntoIterator<Item = A2> + Debug,
    A2: AsRef<OsStr> + Debug,
{
    let login_arguments = env_var_to_arguments("PLUGIN_LOGIN_ARGUMENTS")?;

    println!("INFO: running buildah login --username {username} --password-stdin {arguments:?} {login_arguments:?} {registry}");
    let mut child = Command::new("/usr/bin/buildah")
        .stdin(Stdio::piped())
        .args(["login", "--username", username, "--password-stdin"])
        .args(arguments)
        .args(login_arguments)
        .arg(registry)
        .spawn()?;

    let mut stdin = child.stdin.take().expect("failed to open stdin");

    let password = String::from(password);
    let input_thread = std::thread::spawn(move || {
        stdin
            .write_all(password.as_bytes())
            .expect("Failed to write to stdin");
    });

    let status = child.wait()?;

    input_thread.join().expect("Child thread failed");

    if !status.success() {
        return Err(format!("failed with exit status {status}").into());
    }

    Ok(())
}

fn build<A, A2, T, C>(
    arguments: A,
    tag: T,
    context: Option<C>,
) -> Result<(), Box<dyn std::error::Error>>
where
    A: IntoIterator<Item = A2> + Debug,
    A2: AsRef<OsStr> + Debug,
    T: ToString,
    C: ToString,
{
    let mut build_arguments = env_var_to_arguments("PLUGIN_BUILD_ARGUMENTS")?;

    let tag = tag.to_string();
    build_arguments.push(format!("--tag={tag}"));
    if let Some(context) = context {
        build_arguments.push(context.to_string());
    }

    println!("INFO: running buildah build {arguments:?} {build_arguments:?}");
    let status = Command::new("/usr/bin/buildah")
        .arg("build")
        .args(arguments)
        .args(build_arguments)
        .spawn()?
        .wait()?;

    if !status.success() {
        return Err(format!("failed with exit status {status}").into());
    }

    Ok(())
}

fn push<A, A2, T>(arguments: A, tag: T) -> Result<(), Box<dyn std::error::Error>>
where
    A: IntoIterator<Item = A2> + Debug,
    A2: AsRef<OsStr> + Debug,
    T: ToString,
{
    let mut push_arguments = env_var_to_arguments("PLUGIN_PUSH_ARGUMENTS")?;

    push_arguments.push(tag.to_string());

    println!("INFO: running buildah push {arguments:?} {push_arguments:?}");
    let status = Command::new("/usr/bin/buildah")
        .arg("push")
        .args(arguments)
        .args(push_arguments)
        .spawn()?
        .wait()?;

    if !status.success() {
        return Err(format!("failed with exit status {status}").into());
    }

    Ok(())
}

fn manifest<A, A2, T>(arguments: A, tag: T) -> Result<(), Box<dyn std::error::Error>>
where
    A: IntoIterator<Item = A2> + Debug,
    A2: AsRef<OsStr> + Debug,
    T: ToString,
{
    let manifest_arguments = env_var_to_arguments("PLUGIN_MANIFEST_ARGUMENTS")?;

    let tags: Vec<String> = env::var("PLUGIN_MANIFEST_TAGS")?
        .split(',')
        .map(str::trim)
        .map(String::from)
        .collect();

    let arguments: Vec<_> = arguments.into_iter().collect();
    let tag = tag.to_string();

    {
        let manifest_create_arguments = env_var_to_arguments("PLUGIN_MANIFEST_CREATE_ARGUMENTS")?;
        println!("INFO: running buildah manifest create {arguments:?} {manifest_arguments:?} {manifest_create_arguments:?} {tag}");
        let status = Command::new("/usr/bin/buildah")
            .args(["manifest", "create"])
            .args(&arguments)
            .args(&manifest_arguments)
            .args(manifest_create_arguments)
            .arg(&tag)
            .spawn()?
            .wait()?;

        if !status.success() {
            return Err(format!("failed with exit status {status}").into());
        }
    }

    {
        let manifest_add_arguments = env_var_to_arguments("PLUGIN_MANIFEST_ADD_ARGUMENTS")?;
        for platform_tag in tags {
            println!("INFO: running buildah manifest add {arguments:?} {manifest_arguments:?} {manifest_add_arguments:?} {tag} docker://{platform_tag}");
            let status = Command::new("/usr/bin/buildah")
                .args(["manifest", "add"])
                .args(&arguments)
                .args(&manifest_arguments)
                .args(&manifest_add_arguments)
                .arg(&tag)
                .arg(format!("docker://{platform_tag}"))
                .spawn()?
                .wait()?;

            if !status.success() {
                return Err(format!("failed with exit status {status}").into());
            }
        }
    }

    let manifest_push_arguments = env_var_to_arguments("PLUGIN_MANIFEST_PUSH_ARGUMENTS")?;
    println!("INFO: running buildah manifest push --all {arguments:?} {manifest_arguments:?} {manifest_push_arguments:?} {tag}");
    let status = Command::new("/usr/bin/buildah")
        .args(["manifest", "push", "--all"])
        .args(arguments)
        .args(manifest_arguments)
        .args(manifest_push_arguments)
        .arg(&tag)
        .arg(format!("docker://{tag}"))
        .spawn()?
        .wait()?;

    if !status.success() {
        return Err(format!("failed with exit status {status}").into());
    }
    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let tag = match env::var("PLUGIN_TAG") {
        Err(NotPresent) => {
            println!("ERROR: tag setting is mandatory");
            return Err(NotPresent.into());
        }
        tag => tag?,
    };

    let commands: Vec<String> = match env::var("PLUGIN_COMMANDS") {
        Err(NotPresent) => vec!["login".into(), "build".into(), "push".into()],
        commands => commands?
            .split(',')
            .map(str::trim)
            .map(String::from)
            .collect(),
    };

    let context: Option<String> = match env::var("PLUGIN_CONTEXT") {
        Err(NotPresent) => None,
        context => Some(context?),
    };
    let username: Option<String> = match env::var("PLUGIN_USERNAME") {
        Err(NotPresent) => None,
        username => Some(username?),
    };
    let password: Option<String> = match env::var("PLUGIN_PASSWORD") {
        Err(NotPresent) => None,
        password => Some(password?),
    };
    let registry: Option<String> = match env::var("PLUGIN_REGISTRY") {
        Err(NotPresent) => None,
        registry => Some(registry?),
    };

    let env_files: HashMap<String, String> = match env::var("PLUGIN_ENV_FILES") {
        Err(NotPresent) => Default::default(),
        v => serde_json::from_str(&v?)?,
    };

    for (var_name, destination) in env_files {
        println!("INFO: Writing variable {var_name} to {destination}");
        let var = env::var(var_name)?;
        let destination = PathBuf::from(destination);
        if let Some(parent) = destination.parent() {
            fs::create_dir_all(parent)?;
        }
        fs::write(destination, var)?;
    }

    let arguments = env_var_to_arguments("PLUGIN_ARGUMENTS")?;
    for command in commands {
        match command.as_str() {
            "login" => login(
                &arguments,
                username.as_ref().expect("login needs username"),
                password.as_ref().expect("login needs password"),
                registry.as_ref().expect("login needs registry"),
            )?,
            "build" => build(&arguments, &tag, context.as_ref())?,
            "push" => push(&arguments, &tag)?,
            "manifest" => manifest(&arguments, &tag)?,
            unknown => return Err(format!("Unknown command {unknown}").into()),
        }
    }
    Ok(())
}
